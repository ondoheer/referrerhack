import requests
import subprocess
import os

from flask import Flask, render_template, request, url_for
from bs4 import BeautifulSoup as Soup
import os

app = Flask(__name__)


DEMO = os.environ.get('DEMO', False)
DEMO_URL = 'https://recurse.zulipchat.com/'

@app.route('/')
def index():
	"""
	Grab the original site URL and try to get it's login page. 
	If it exists, we will replace it with it and add a nice warning to it.
	
	Much more could be done here, check what it was build with and find it's standard login form 
	for example. as wp-login.php would be for WP
	"""


	return render_template('index.html',
							replacement_url=url_for('login', _external=True)
	)





@app.route('/login')
def login():
	"""
	Renders the newly downloaded template
	"""
	
	# We check if the link was accessed through a malicious anchor tag
	referrer = request.args.get('referrer', None) if not DEMO else DEMO_URL # THIS IS THE DOMAIN THE USER COMES FROM


	if DEMO or referrer:
		
		# get CWD so we can start manipulating files in there
		cwd = os.getcwd()
		DOMAIN = referrer.split('/')[2] if not DEMO else DEMO_URL.split('/')[2] # clean domain name
		
		# We try to see if the domain has a login endpoint
		login_route = requests.get(referrer + 'login')

		
		if login_route.status_code in [200, 301, 302]: # if the login page exists

			INDEX_PATH = '{}/app/templates/logins/{}'.format(cwd, DOMAIN) # where our files will be stored

			# create the login folder for this domain if it doesn't exist yet and clone the website
			if not os.path.exists(INDEX_PATH):
				os.makedirs(INDEX_PATH)
				print(referrer)
				## download the file to the desired directory
				## we replace all of the relative URL with the real ones so we can load the css and js required faster
				wget = subprocess.call(['wget',
										# 'E',
										# 'H',
										'--convert-links',
										# 'K',
										# 'p'
										'robots=off',
										
										referrer + 'login'],
									cwd='app/templates/logins')
			
			
				# wget download a basic file without extension, so we have to rename it
				os.rename('{}/{}login'.format(cwd, 'app/templates/logins/'), '{}/index.html'.format(INDEX_PATH))

				# we open and modify the path
				with open('{}/index.html'.format(INDEX_PATH), 'r') as temp_file:

					website = Soup(temp_file, 'html.parser') 
					website.body.append(Soup("""<div style="position: fixed;bottom: 0;z-index: 99999999;background: red;margin: 0;width: 100%;text-align: center;color: #D8000C;background-color: #FFD2D2;padding: 25px;">
													<h4>This is not the real site. This could be phishing!</h4>
													<p>It is this easy to trick you into giving your credentials. You should always look the URL before login in.</p>
												</div>"""))

				# We get a new index with the injected script
				with open(INDEX_PATH + '/index.html', 'w+') as index:
					index.write(website.html.prettify())

			# Add crappy script to file
			
			replacement_url=url_for('login', _external=True)
			return render_template(
			'logins/{}/index.html'.format(DOMAIN),
			replacement_url=replacement_url
		)

		
	return render_template(
		'lost.html'		
	)
	
	